export class CreateAnimalDto {
  name: string;
  species: string;
  scientificName: string;
}
