import { Injectable } from '@nestjs/common';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { UpdateAnimalDto } from './dto/update-animal.dto';
import { Repository } from 'typeorm';
import { Animal } from './entities/animal.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AnimalService {
  constructor(
    @InjectRepository(Animal)
    private animalRepository: Repository<Animal>,
  ) {
    //construtor vazio
  }

  async create(createAnimalDto: CreateAnimalDto): Promise<Animal> {
    return await this.animalRepository.save(createAnimalDto);
  }

  findAll(): Promise<Animal[]> {
    return this.animalRepository.find();
  }

  findOne(id: number): Promise<Animal> {
    return this.animalRepository.findOneBy({ id });
  }

  async update(id: number, updateAnimalDto: UpdateAnimalDto): Promise<Animal> {
    updateAnimalDto.id = id;
    return await this.animalRepository.save(updateAnimalDto);
  }

  async remove(id: number) {
    return await this.animalRepository.delete(id);
  }
}
