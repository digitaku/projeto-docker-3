
namespace teste2.Entities;

public class Produto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public double Price { get; set; }
}