using teste2.Data;
using teste2.Entities;

namespace teste2.Repositories;
public class ProdutoRepository : IProdutoRepository
{
    private readonly ProdutoContext _context;

    public ProdutoRepository(ProdutoContext context)
    {
        _context = context;
    }


    public List<Produto> GetAllProducts()
    {
        return _context.Produtos.ToList();
    }

    public Produto GetProductById(int id)
    {
        return _context.Produtos.Where(p => p.Id == id).First();
    }

    public void CreateProduct(Produto produto)
    {
        _context.Add(produto);
        _context.SaveChanges();
    }
    public void UpdateProduct(Produto produto)
    {
        _context.Update(produto);
        _context.SaveChanges();
    }

    public void DeleteProduct(Produto produto)
    {
        _context.Remove(produto);
        _context.SaveChanges();
    }
}