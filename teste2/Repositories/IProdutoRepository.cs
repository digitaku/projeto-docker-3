using teste2.Entities;

namespace teste2.Repositories;
public interface IProdutoRepository
{
    public List<Produto> GetAllProducts();
    public Produto GetProductById(int id);
    public void CreateProduct(Produto produto);
    public void UpdateProduct(Produto produto);
    public void DeleteProduct(Produto id);
}