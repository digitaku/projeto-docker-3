
namespace teste2.Dto;

public class ProdutoCreateUpdateDTO
{
    public string? Name { get; set; }
    public double Price { get; set; }
}