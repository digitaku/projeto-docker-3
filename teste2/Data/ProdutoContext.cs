using Microsoft.EntityFrameworkCore;
using teste2.Entities;

namespace teste2.Data;

public class ProdutoContext : DbContext
{
      protected readonly IConfiguration Configuration;
     public ProdutoContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to sql server database
        options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
    }
    public DbSet<Produto> Produtos { get; set; }

}