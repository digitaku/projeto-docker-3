using Microsoft.AspNetCore.Mvc;
using teste2.Entities;
using teste2.Dto;
using teste2.Services;

namespace teste2.Controllers;

[ApiController]
[Route("[controller]")]
public class ProdutoController : ControllerBase
{

    private readonly ProdutoService _services;
    public ProdutoController(ProdutoService services)
    {
        this._services = services;

    }
    [HttpGet()]
    public IEnumerable<Produto> Get()
    {
        return this._services.GetAll();
    }

    [HttpGet("{id}")]
    public Produto GetById(int id)
    {
        return this._services.GetOneById(id);
    }

    [HttpPost()]
    public Produto Create(ProdutoCreateUpdateDTO dto)
    {
        return this._services.Create(dto);
    }

    [HttpPatch("{id}")]
    public Produto Update(int id, ProdutoCreateUpdateDTO dto)
    {
        return this._services.Update(id, dto);
    }

    [HttpDelete("{id}")]
    public String Delete(int id)
    {
        this._services.Delete(id);
        return "DELETED";
    }
}