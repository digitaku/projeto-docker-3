using teste2.Entities;
using teste2.Dto;
using teste2.Repositories;

namespace teste2.Services;

public class ProdutoService
{
    private readonly IProdutoRepository _repository;
    public ProdutoService(IProdutoRepository repository)
    {
        _repository = repository;
    }
    public IEnumerable<Produto> GetAll()
    {
        return _repository.GetAllProducts();
    }

    public Produto GetOneById(int id)
    {
        return _repository.GetProductById(id);
    }

    public Produto Create(ProdutoCreateUpdateDTO dto)
    {
        Produto produto = new Produto();
        produto.Name = dto.Name;
        produto.Price = dto.Price;

        _repository.CreateProduct(produto);

        return produto;
    }

    public Produto Update(int id, ProdutoCreateUpdateDTO dto)
    {
        Produto update = _repository.GetProductById(id);
        update.Name = dto.Name;
        update.Price = dto.Price;
        _repository.UpdateProduct(update);
        return update;
    }

    public void Delete(int id)
    {
        Produto produto = _repository.GetProductById(id);
        _repository.DeleteProduct(produto);
    }

}