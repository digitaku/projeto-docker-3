CREATE TABLE produto.dbo.Produtos (
	Id int NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Price float NOT NULL,
	CONSTRAINT PK_Produtos PRIMARY KEY (Id)
);